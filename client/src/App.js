import React, { Component } from 'react'
import Spaces from './containers/Spaces'

class App extends Component {
  render() {
    return (
      <Spaces />
    )
  }
}

export default App
