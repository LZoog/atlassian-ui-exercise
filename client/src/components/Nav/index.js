import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

const Nav = (props) => (
  <nav>
    <ul>
      {props.spaces.map(space => (
        <li key={space.sys.id}>
          <NavLink to={{ pathname: `/spaceexplorer/${space.sys.id}`}}>
            {space.fields.title}
          </NavLink>
        </li>
      ))}
    </ul>
  </nav>
)

export default Nav

Nav.propTypes = {
  spaces: PropTypes.arrayOf(PropTypes.shape({
    sys: PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired,
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired
    }).isRequired,
  })).isRequired
}
