import React from 'react'
import PropTypes from 'prop-types'

const Table = () =>
  <table>
    <tr>
      {/* headers.map */}
      <th>Title</th>
      <th>Summary</th>
      <th>Created By</th>
      <th>Updated By</th>
      <th>Last Updated</th>
    </tr>
    <tr>
      {/* rows.map */}
      <td>{title}</td>
      <td>{summary}</td>
      <td>{createdBy}</td>
      <td>{updatedBy}</td>
      <td>{lastUpdated}</td>
    </tr>
  </table>

// Table.propTypes = {
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   author: PropTypes.string.isRequired,
// }

export default Table