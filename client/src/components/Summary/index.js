import React from 'react'
import PropTypes from 'prop-types'

const Summary = ({title, description, author}) =>
  <div>
    <h2>{title}</h2>
    <div>{author}</div>
    <div>{description}</div>
  </div>

Summary.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
}

export default Summary
