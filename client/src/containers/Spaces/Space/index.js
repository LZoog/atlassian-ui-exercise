import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import Summary from '../../../components/Summary'

export default class Space extends Component {
  state = {
    space: [],
    author: '',
    error: false,
    entries: [],
    assets: []
  }

  componentDidMount() {
    this.getSpace(this.props.match.params.id)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.getSpace(this.props.match.params.id)
    }
  }

  async getSpace(id) {
    // to do: these guys (excluding 'author') can run in parallel
    const space = await axios.get(`/space/${id}`)
      .catch(() => { this.setState({ error: true }) } )

    const author = await axios.get(`/users/${space.data.sys.createdBy}`)
      .catch(() => { this.setState({ error: true }) } )

    const entries = await axios.get(`/space/${id}/entries`)
      .catch(() => { this.setState({ error: true }) } )

    const assets = await axios.get(`/space/${id}/assets`)
      .catch(() => { this.setState({ error: true }) } )

    this.setState({ space: space.data, author: author.data.fields.name, entries: entries.data.items, assets: assets.data.items })
  }

  render() {
    const { space, author, } = this.state


    let output = 'This space not available.' // this never renders because /space/anything returns the first entry
    if (author) {
      output = <Summary
        title={space.fields.title}
        description={space.fields.description}
        {...{author}}
      />
    }

    return(
      <div>
        {output}
      </div>
    )
  }

}

Space.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
  }).isRequired
}
