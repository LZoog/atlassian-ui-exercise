import React, { Component } from 'react'
import axios from 'axios'
import { Redirect, Route, Switch } from 'react-router-dom'
import Space from './Space'
import Nav from '../../components/Nav'

export default class Spaces extends Component {
  state = {
    spaces: [],
    error: false,
    loaded: false
  }

  async componentDidMount() {
    const spaces = await axios.get('/space')
      .catch(() => { this.setState({ error: true }) } )
    this.setState({ spaces: spaces.data.items, loaded: true })
  }

  render() {
    const { spaces, loaded } = this.state
    // render error if error

    return(
      <main>
        <Nav {...{spaces}} />
        
        <Switch>
          <Route path='/spaceexplorer/:id' component={Space} />
          {loaded && <Redirect to={`/spaceexplorer/${spaces[0].sys.id}`} />}
        </Switch>

      </main>
    )
  }

}

