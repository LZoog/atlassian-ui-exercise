
# Atlassian UI Exercise

## Stack

- Node.JS & NPM
- Express.JS
- RAML & osprey
- React & react-router-dom, webpack & babel from `create-react-app`
- SASS (might be overkill, will implement if needed)
- Jest & Enzyme
- ESLint

## How to Run
These instructions assume you have `nvm` (Node Version Manager) installed. Please visit http://nvm.sh for installation directions if you do not have this installed.

-add instructions for cloning & cd'ing into repo-

This application was developed with NodeJS version `10.15.3`. Check the Node version you are running with `node -v`. If the output does not reflect `10.15.3`, try running:
`nvm use 10.15.3`

If this Node version is not installed, install it with:
`nvm install 10.15.3`

The new installation of Node should be automatically used. Then, install application dependencies:
`npm install`

And finally, run the application:
`npm start`

You can then visit `localhost:3000` in your browser.

## Building
To do. Easily fixed, but we don't want two servers running.