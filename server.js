const express = require('express')
const mockService = require('osprey-mock-service')
const port = process.env.PORT || 3001


// if (process.env.NODE_ENV === 'production') {
//   app.use(express.static('client/build'))
// }

let app
mockService.loadFile('space-api.raml')
  .then(mockApp => {
    app = express()
    app.use(mockApp)
    app.listen(port)
  })
